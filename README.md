# LeJeuDeLaGuerre-Kriegsspiel

|War1|KriegsspielComm|
|:---:|:---:|
![alt text](https://www.lirmm.fr/%7Esuro/godotKriegspiel/war1.jpeg "war1.jpeg") | ![alt text](https://www.lirmm.fr/%7Esuro/godotKriegspiel/kriegspielComm.png "KriegsspielComm.png")

## Sujet

Réalisez une implémentation du jeu de plateau "le jeu de la guerre" dans le moteur de jeu <font color="red">[Godot](https://godotengine.org/)</font>. "Le jeu de la guerre" est un kriegsspiel basé sur les guerres du 18e siècle qui représente l'affrontement d'unités d'infanterie, de cavalerie et d'artillerie. Une des particularités du jeu est la nécessité de maintenir une ligne de communication entre son camp (les arsenaux) et les troupes au moyen d'unités de communication.

## Objectifs

Le but du projet est un simple mode joueur contre joueur au tour par tour sur la même machine.
Les principaux axes de travail sont:

* interface, sélection, déplacement.
* suggestions, avertissement, et information sur les actions possibles en fonction des règles.
* résolution des attaques, règles des communications, conditions de Victoire.
* modularité dans la conception du programme, chargement de maps, de mod graphiques, règles alternatives.

|KriegsspielGodot|
|:---:|
|![alt text](https://www.lirmm.fr/%7Esuro/godotKriegspiel/kriegspielGodot.png "KriegsspielGodot.png")|

## Quelque image du jeu

Ci-dessous quelques captures d'écran.

|V1|
|:---:|
|![Plateau de jeu](http://gitlab.com/R3dNB2ll/lejeudelaguerre-kriegspiel/-/blob/master/image/image1.png "Un plateau de jeu")|



|V2|
|:---:|
|![Plateau de jeu](http://gitlab.com/R3dNB2ll/lejeudelaguerre-kriegspiel/-/blob/master/image/image2.png "Un plateau de jeu")|
|![Attaque 1](http://gitlab.com/R3dNB2ll/lejeudelaguerre-kriegspiel/-/blob/master/image/image3.png "Une attaque")|
|![Attaque 2](http://gitlab.com/R3dNB2ll/lejeudelaguerre-kriegspiel/-/blob/master/image/image4.png "Une attaque 2")|


## Vidéo de présentation

[![Vidéo de présentation](http://img.youtube.com/vi/arJky6cg_CQ/0.jpg)](http://www.youtube.com/watch?v=arJky6cg_CQ)

## Référence

Le texte complet des règles est fourni <font color="red">[ICI](https://www.lirmm.fr/%7Esuro/godotKriegspiel/kriegsspiel.pdf)</font> (extrait de :<font color="red">[Le jeu de la guerre releve des positions successives de toutes les forces au cours d'une partie](https://livre.fnac.com/a1819917/Guy-Debord-Le-jeu-de-la-guerre-releve-des-positions-successives-de-toutes-les-forces-au-cours-d-une-partie)</font>)

Le moteur Godot propose la programmation soit en C# soit en godotScript (un simili python).<font color="red">[Documentation](https://docs.godotengine.org/en/3.1/getting_started/scripting/index.html)</font>

Toutes les ressources graphiques sont fournies (modèles 3D).

Une preuve de concept de l'interface 3d du projet existe (écrite en godotscript) <font color="red">[lien](https://www.lirmm.fr/%7Esuro/godotKriegspiel/kriegspiel.zip)</font>
