using Godot;
using System;


/// <summary>
/// A datasheet is used to process the real unit stats against another unit. If a unit is subject to a passive, the datasheet will change accordingly.
/// Before making any action, the unit will check its datasheet to see if it has any bonuses
/// </summary>
public class Datasheet {
    //Those are the modifiers that the unit is subject to, _attack = 5 means that we add 5 damages to all of its attacks
    private int _attack;    public int Damage { get => _attack; }
    private int _defense;   public int Defense { get => _defense; }
    private int _movement;  public int Movement { get => _movement; }
    private bool _surrend;  public bool Surrend { get => _surrend; set => _surrend = value;}
    private bool _immune;   public bool Immune { get => _immune; set => _immune = value;}

    public Datasheet () {
        reset();
    }

    public void addAttack (int n) {
        _attack += n;
    }

    public void addDefense (int n) {
        _defense += n;
    }

    public void addMovement (int n) {
        _movement += n;
    }

    
    /// <summary>
    /// this should be called at the very beggining of each player turn
    /// </summary>
    public void reset () {
        _attack = 0;
        _defense = 0;
        _movement = 0;
        _surrend = false;
        _immune = false;
    }

    public String toString () {
        String ans = "Datasheet: Attack : " + _attack + '\n';
              ans += "           Defense : " + _defense + '\n';
              ans += "           Movement : " + _movement + '\n';
              ans += "           Surrend : " + _surrend + '\n';
              ans += "           Immune : " + _immune + '\n';
        return ans;
    }
}
