using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// An Animation is a class that stores animation data about an unit
/// </summary>
public class SpriteRenderer : Renderer {
    public enum ERenderType {
        Unit,
        Terrain,
        Effect,
        TerrainBillboard
    }

    private SpriteFrames _spriteFrames;
    private AnimatedSprite3D _animatedSpriteNode;
    private ERenderType _type = ERenderType.Unit;
    private List<SpriteData> _spriteDataList = new List<SpriteData>();
    private String _defaultAnimation = null;
    private bool _randomrotation = false;
    private bool _randomflip = false;
    private static ShaderMaterial _shader = (ShaderMaterial)GD.Load("res://shaders/cloudy.tres");
    private static ShaderMaterial _shaderbb = (ShaderMaterial)GD.Load("res://shaders/cloudy_bb.tres");
    private static ShaderMaterial _shadereffect = (ShaderMaterial)GD.Load("res://shaders/effect.tres");

    private float _frequency = 2f;

    private static Random _random = new Random(0); //Used to start the animations at a random frame

    public struct SpriteData : IRendererData {
        private int _animationSpeed;    public int GetAnimationSpeed {get => _animationSpeed;}
        private int _spriteSize;        public int GetSpriteSize {get => _spriteSize;}
        private Image _image;           public Image GetImage {get => _image;}
        private String _animationName;   public String GetAnimationName {get => _animationName;}
        

        public SpriteData (int animationSpeed, int spriteSize, Image image, String animationName) {
            _animationSpeed = animationSpeed;
            _spriteSize = spriteSize;
            _image = image;
            _animationName = animationName;
        }
    }

    public SpriteRenderer (SpriteRenderer renderer, String defaultAnimation) : base(renderer) {
        Name = "RENDERER";
        _frequency = renderer._frequency;
        _animatedSpriteNode = new AnimatedSprite3D();
        _spriteFrames = new SpriteFrames();
        _type = renderer._type;
        _spriteDataList = new List<SpriteData>(renderer._spriteDataList);
        _randomrotation = renderer._randomrotation;
        _randomflip = renderer._randomflip;
        _defaultAnimation = renderer._defaultAnimation;

        _animatedSpriteNode.Frames = _spriteFrames;
        _animatedSpriteNode.Centered = false;
        _animatedSpriteNode.Play();
        //_animatedSpriteNode.Shaded = false; //Shadows etc

        AddChild(_animatedSpriteNode);

        foreach(SpriteData sd in _spriteDataList) {
            AddAnimation(sd, sd.GetAnimationName);
        }
        
        PlayAnimation(defaultAnimation);
        
        if (_type == ERenderType.Terrain) {
            _animatedSpriteNode.MaterialOverride = _shader;
            _animatedSpriteNode.AlphaCut = SpriteBase3D.AlphaCutMode.Disabled;
            _animatedSpriteNode.RotationDegrees = new Vector3(90, 0 ,0);

            if (_randomflip) {
                _animatedSpriteNode.FlipH = _random.Next(0, 2) == 1;
            }
            if (_randomrotation) {
                _animatedSpriteNode.RotationDegrees += new Vector3(0, 90 * _random.Next(0, 4), 0);
            }

            _animatedSpriteNode.Centered = true;
            int frames = _animatedSpriteNode.Frames.GetFrameCount(_animatedSpriteNode.Animation);
            int frame = _random.Next(0, frames);
            _animatedSpriteNode.Frame = frame;
        } else if (_type == ERenderType.Unit) {
            _animatedSpriteNode.MaterialOverride = _shaderbb;
            _animatedSpriteNode.AlphaCut = SpriteBase3D.AlphaCutMode.Discard; //Discard should not work with the shadows enabled, but is way faster
            //_animatedSpriteNode.Billboard = SpatialMaterial.BillboardMode.FixedY;
        } else if (_type == ERenderType.Effect) {
            _animatedSpriteNode.MaterialOverride = _shadereffect;
            _animatedSpriteNode.AlphaCut = SpriteBase3D.AlphaCutMode.Discard;
            _animatedSpriteNode.RotationDegrees = new Vector3(90, 0 ,0);
            _animatedSpriteNode.Centered = true;
            Hide();
        } else if (_type == ERenderType.TerrainBillboard) {
            _animatedSpriteNode.MaterialOverride = _shaderbb;
            //Frequency check
            if ((float)_random.NextDouble() > _frequency) {
                QueueFree();
            }
            _animatedSpriteNode.AlphaCut = SpriteBase3D.AlphaCutMode.Discard;
            int frames = _animatedSpriteNode.Frames.GetFrameCount(_animatedSpriteNode.Animation);
            int frame = _random.Next(0, frames);
            _animatedSpriteNode.Frame = frame;
            //_animatedSpriteNode.Billboard = SpatialMaterial.BillboardMode.FixedY;
            if (_randomflip) {
                _animatedSpriteNode.FlipH = _random.Next(0, 2) == 1;
            }
        }
    }

    /// <summary>
    /// Instantiate Animation
    /// </summary>
    /// <param name="animatedSpriteNode"> the <see cref="AnimatedSprite3D"/> node of the unit </param>
    public SpriteRenderer () : base() {
        _animatedSpriteNode = new AnimatedSprite3D();
        _spriteFrames = new SpriteFrames();
        _animatedSpriteNode.Frames = _spriteFrames;

        AddChild(_animatedSpriteNode);
    }

    public override bool Config(Godot.Collections.Dictionary rendererDict, String path, String animationName) {
        int spriteSize = -1;
        int animationSpeed = 0;

        //SpriteSize
		if (!rendererDict.Contains("spritesize") || !(rendererDict["spritesize"] is float)) {
			System.Console.WriteLine("Error : renderer.spritesize@" + path);
			return false;
		}
		spriteSize = (int)(float)rendererDict["spritesize"];
        if (spriteSize <= 1) {
            System.Console.WriteLine("Error : renderer.spritesize.value@" + path);
			return false;
        }

        //AnimSpeed
		if (!rendererDict.Contains("animspeed") || !(rendererDict["animspeed"] is float)) {
			System.Console.WriteLine("Error : renderer.animspeed@" + path);
			return false;
		}
		animationSpeed = (int)(float)rendererDict["animspeed"];
        if (animationSpeed < 0) {
            System.Console.WriteLine("Error : renderer.animspeed.value@" + path);
			return false;
        }

        //Type (optionnal)
		if (rendererDict.Contains("type") && (rendererDict["type"] is String)) {
            ERenderType type = StrToERenderType((String)rendererDict["type"]);
            if ((int)type == -1) {
                System.Console.WriteLine("Error : renderer.type.value@" + path);
                return false;
            }
            _type = type;
		}

        //RandomRotation (optionnal)
		if (rendererDict.Contains("randomrotation") && (rendererDict["randomrotation"] is bool)) {
            _randomrotation = (bool)rendererDict["randomrotation"];
		}

        //RandomFlit (optionnal)
		if (rendererDict.Contains("randomflip") && (rendererDict["randomflip"] is bool)) {
            _randomflip = (bool)rendererDict["randomflip"];
		}

        //Specific to types
        if (_type == ERenderType.TerrainBillboard) {
            if (rendererDict.Contains("frequency") && (rendererDict["frequency"] is float)) {
                _frequency = (float)rendererDict["frequency"];
            }
        }
        

        ///IMAGE
        String filePath = System.IO.Path.Combine(new String[] {path, animationName + ".png"});
        //var check
        if (animationName == null || animationName.Equals("")) {
            Console.WriteLine("Error : renderer.animationname(is empty or null)@" + path);
            return false;
        }
        //file check
        if (!System.IO.File.Exists(filePath)) {
            Console.WriteLine("Error : File not found:" + filePath + " @" + path);
            return false;
        }
        //load check
        Image image = new Image();
        if(image.Load(filePath) != Error.Ok) {
            Console.WriteLine("Error : Could not load:" + filePath + " @" + path);
            return false;
        }

        SpriteData spriteData = new SpriteData(animationSpeed, spriteSize, image, animationName);
        AddAnimation(spriteData, animationName);
        _spriteDataList.Add(spriteData);

        return true;
    }

    public static ERenderType StrToERenderType (String type) {
        switch (type) {
            case "Unit":
                return ERenderType.Unit;
            case "Terrain":
                return ERenderType.Terrain;
            case "Effect":
                return ERenderType.Effect;
            case "TerrainBillboard":
                return ERenderType.TerrainBillboard;
            default:
                return (ERenderType)(-1);
        }
    }

    /// <summary>
    /// Adds a new animation to the <see cref="AnimatedSprite3D"/> node related to the unit
    /// </summary>
    /// <param name="data"> the animation data</param>
    /// <param name="title"> the animation title, used to play that animation later </param>
    public override void AddAnimation(IRendererData data, String title) {
        if (_defaultAnimation == null)
            _defaultAnimation = title;

        SpriteData spriteData = (SpriteData)data;
        String animationName = title;
        _spriteFrames.AddAnimation(animationName);
        _spriteFrames.SetAnimationSpeed(animationName, spriteData.GetAnimationSpeed);

        int framesNb = (int)spriteData.GetImage.GetSize().y / spriteData.GetSpriteSize;
        for(int i = 0; i < framesNb; i++) {
            // We get an image at [0, i*spriteSize] of the size spriteSize, make it an ImageTexture and add it to the animation
            Image currentFrame = spriteData.GetImage.GetRect(new Rect2(new Vector2(0, i * spriteData.GetSpriteSize), new Vector2(spriteData.GetSpriteSize, spriteData.GetSpriteSize)));
            
            ImageTexture imageTextureFrame = new ImageTexture();
            imageTextureFrame.CreateFromImage(currentFrame);
            imageTextureFrame.SetStorage(ImageTexture.StorageEnum.CompressLossless);
            imageTextureFrame.SetFlags(0);

            _spriteFrames.AddFrame(animationName, imageTextureFrame);
        }
    }

    /// <summary>
    /// Plays an existing animation
    /// </summary>
    /// <param name="animationName"> the animation title </param>
    public override void PlayAnimation (String animationName) {
        float spriteSize = _spriteFrames.GetFrame(animationName, 0).GetSize().x;
        _animatedSpriteNode.PixelSize = 1 / spriteSize;
        if (_type == ERenderType.Unit || _type == ERenderType.TerrainBillboard)
            _animatedSpriteNode.Offset = new Vector2(-spriteSize / 2, 0);
        _animatedSpriteNode.Play(animationName);
        Show(true);
    }

    // /// <summary>
    // /// Plays an attack animation, composed of 3 animations. first, the idle animation, then the attack, then the idle animation again.
    // /// </summary>
    // /// <param name="attackName"> the attack animation title </param>
    private async void PlayAttack (String attackName) {
        float idleAnimationTime = _spriteFrames.GetAnimationSpeed("idle") * _spriteFrames.GetFrameCount("idle");
        float attackAnimationTime = _spriteFrames.GetAnimationSpeed(attackName) * _spriteFrames.GetFrameCount(attackName);

        Timer t = new Timer();
        
        PlayAnimation("idle");
        t.SetWaitTime(idleAnimationTime);
        await ToSignal(t, "timeout");

        PlayAnimation(attackName);
        t.SetWaitTime(attackAnimationTime);
        await ToSignal(t, "timeout");

        PlayAnimation("idle");
    }

    public override void Show (bool b) {
        if (b) {
            _animatedSpriteNode.SetFrame(0);
            Show();
            _animatedSpriteNode.PauseMode = Node.PauseModeEnum.Process;
        }
            
        else {
            Hide();
            _animatedSpriteNode.PauseMode = Node.PauseModeEnum.Stop;
        }
    }

    public override Renderer Clone (String defaultAnimation) {
        return new SpriteRenderer(this, defaultAnimation);
    }

    public override void Skin (int team) {
        if (_type != ERenderType.Unit) {
            return;
        }

        switch (team) {
            case 0:
                //Colorier en bleu
                _animatedSpriteNode.Set("modulate", new Color(0.5f,0.5f,1));
                _animatedSpriteNode.FlipH = true; 
                break;

            case 1:
                _animatedSpriteNode.Set("modulate", new Color(1,0.5f,0.5f));
                //Colorier en rouge
                break;

            case 2:
                _animatedSpriteNode.Set("modulate", new Color(1,1,0.5f));
                _animatedSpriteNode.FlipH = true;
                //Colorier en jaune
                break;

            case 3:
                _animatedSpriteNode.Set("modulate", new Color(1,0.5f,0.5f));
                //Colorier en vert
                break;

            default:
                break;
        }
    }
} 
