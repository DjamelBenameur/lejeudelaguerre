using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// An Attack is a class that stores data about an attack, and process witch unit can be damaged by its parent unit.
/// </summary>
public class Attack : Action {
	private static List<Attack> _attackList = new List<Attack>();
	private int _damage;    public int GetDamage { get => _damage; }

  private List<Unit> _bakedUnitList = new List<Unit>(); public List<Unit> BakedUnitList { get => _bakedUnitList; }
	private List<Unit> _unbakedUnitList = new List<Unit>(); public List<Unit> UnbakedUnitList { get => _unbakedUnitList; }
	private bool _baked = false; public bool Baked { get => _baked; set => _baked = value; }
	private List<Tile> _tileRange = null;
	/// <summary>
	/// Instantiate an attack as a child of a unit
	/// </summary>
	public Attack (Attack attack, Unit parentUnit)
	: base (attack, parentUnit) {
		_damage = attack._damage;
	}

	/// <summary>
	/// Instantiate an Attack
	/// </summary>
	/// <param name="title"> the title of the attack</param>
	/// <param name="description"> the descriprion of the attack</param>
	/// <param name="filter"> a filter list used to check if an attack can hit a targer. we can imagine an attack 'grenade' that can only hit targets with the tag 'ground'</param>
	/// <param name="shape"> the <see href="Shape"> used to check if a target is at range</param>
	/// <param name="damage"> the base damage of the attack</param>
	public Attack (String title, String description, List<String> filter, Shape shape, ETarget filterIgnore, List<String> blockedBy, ETarget blockedByIgnore, Unit parentUnit
	, int damage)
	: base(title, description, filter, shape, filterIgnore, blockedBy, blockedByIgnore, parentUnit) {
		_damage = damage;
		_attackList.Add(this);
	}

	/// <summary>
	/// Returns the attack named "title"
	/// </summary>
	/// <param name="title"> the title of the attack to return</param>
	public static Attack GetAttack(String title) {
		Attack ans = null;

		foreach(Attack a in _attackList) {
			if (a.GetTitle.Equals(title, StringComparison.OrdinalIgnoreCase)) {
				ans = a;
				break;
			}
		}

		return ans;
	}

	/// <summary>
	/// Returns true if this attack can help the unit u
	/// </summary>
	public bool Helps (Unit u) {
		if (!GetParentUnit.IsAliedTo(u))
			return false;
		if (!Baked) {
			Bake();
		}
		if (BakedUnitList.Contains(u))
			return true;
		return false;
	}

	/// <summary>
	/// Returns true if this attack can hit the unit u
	/// </summary>
	public bool Hits (Unit u) {
		if (GetParentUnit.IsAliedTo(u))
			return false;
		if (!Baked) {
			Bake();
		}
		if (BakedUnitList.Contains(u))
			return true;
		return false;
	}

	public String toString () {
		String ans = base.ToString() + '\n';
		ans += "Damage: " + _damage;
		return ans;
	}


	/// <summary>
	/// prints the attacks
	/// </summary>
	public static void printAttacks () {
		foreach(Attack a in _attackList) {
			Console.WriteLine(a);
		}
	}

	/// <summary>
	/// Returns the list of unit that can be hit by this attack
	/// </summary>
	public override List<Unit> SelectRange () {
		if (!Baked)
			Bake();
		return BakedUnitList;
	}

	public List<Tile> TileRange () {
		if (_tileRange == null) {
			_tileRange = GetRangeShape.TileRange(GetParentUnit);
		}
		return _tileRange;
	}


	//*****BAKING THINGS
	/// <summary>
	/// Bakes the unbaked units list
	/// </summary>
	public void Bake () {
		System.Console.WriteLine("baking attack " + GetTitle);
		List<Unit> tmpUnbakedList = new List<Unit>(UnbakedUnitList);
		foreach (Unit u in tmpUnbakedList) {
			if (GetRangeShape.SelectPointOfViewOf(this, u))
				if (!BakedUnitList.Contains(u))
					BakedUnitList.Add(u);
			UnbakedUnitList.Remove(u);
		}
		Baked = true;
	}

	/// <summary>
	/// empty the baked list and fills the unbaked one, should be called for example when this action parent unit moved, or when a blocking unit entered or leaved the range of the action
	/// </summary>
	public void Unbake () {
		_baked = false;
		_bakedUnitList.Clear();
		_unbakedUnitList = new List<Unit>(GetTargetableUnitList);
		_tileRange = null;
	}

	/// <summary>
	/// removes a unit from the baked list, and adds it into the unbaked one, should be called for example when the unit u moved
	/// </summary>
	public void Unbake (Unit u) {
		_baked = false;
		_bakedUnitList.Remove(u);
		if (!_unbakedUnitList.Contains(u))
			_unbakedUnitList.Add(u);
	}

	/// <summary>
	/// Initializes all of the arrays used to target units, should be called only once, at the beggining of the game, when all of the units are present on the board
	/// </summary>
	public override void InitialiseTargetableUnits () {
		base.InitialiseTargetableUnits();
		_unbakedUnitList.Clear();
		_bakedUnitList.Clear();

		GetParentUnit.Unbake();
	}

	/// <summary>
	/// removes a unit from the target lists. Should be called when u is killed
	/// </summary>
	public override void RemoveUnitFromTargets (Unit u) {
		base.RemoveUnitFromTargets(u);
		_bakedUnitList.Remove(u);
		_unbakedUnitList.Remove(u);
	}

	/// <summary>
	/// called when a unit u is killed, if u is inside of the action range, we should unbake the action
	/// </summary>
	public void RemoveBlockingUnit (Unit u) {
		if (GetRangeShape.inShape(GetParentUnit.GetCurrentTile, u.GetCurrentTile)) {
			Unbake();
		}
	}
}
