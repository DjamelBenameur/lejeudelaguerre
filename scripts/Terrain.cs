using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// A Landscape is the representation of terrain on the map, TODO
/// </summary>
public class Terrain : EntityPhysical {
    private static List<Terrain> _terrainList = new List<Terrain>();
    private List<Passive> _passiveList; public List<Passive> GetPassiveList { get => _passiveList; }
    private Tile _currentTile;  public Tile CurrentTile { get => _currentTile; }
    private Renderer _renderer;
    private Renderer _billboardRenderer = null;

    public Terrain (Terrain terrain) : base(terrain) {
        _passiveList = terrain._passiveList;
        _renderer = new SpriteRenderer((SpriteRenderer)terrain._renderer, "terrain");

        AddChild(_renderer);
        if (terrain._billboardRenderer != null){
            _billboardRenderer = new SpriteRenderer((SpriteRenderer)terrain._billboardRenderer, "billboard");
            AddChild(_billboardRenderer);
        }
    }

    public Terrain (String title, String description, ETeam team, List<String> tag, List<Passive> passiveList, Renderer renderer, Renderer billboardRenderer)
    : base (title, description, team, tag) {
        _passiveList = passiveList;
        _renderer = renderer;
        _terrainList.Add(this);
        _billboardRenderer = billboardRenderer;

        if (_billboardRenderer != null)
            AddChild(_billboardRenderer);
        AddChild(renderer);
    }


    /// <summary>
    /// Returns the terrain named "title"
    /// </summary>
    /// <param name="title"> the title of the terrain to return</param>
    public static Terrain GetTerrain(String title) {
        Terrain ans = null;

        foreach(Terrain t in _terrainList) {
            // Console.WriteLine("Testing :" + t.GetTitle + ":");
            if (t.GetTitle.Equals(title)) {
                ans = t;
                break;
            }
        }

        

        return ans;
    }

    public void SetCurrentTile (Tile tile) {
        if (_currentTile != null) {
            _currentTile.RemoveChild(this);
        }
        _currentTile = tile;
        tile.AddChild(this);
    }

    public String toString () {
        String ans = base.ToString() + '\n';
        foreach(Passive p in _passiveList) {
            ans += p.ToString() + '\n';
        }
        return ans;
    }

    public static void printTerrains () {
        foreach(Terrain t in _terrainList) {
            //Console.WriteLine(t);
        }
    }
}
