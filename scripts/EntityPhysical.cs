using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// An EntityPhysical is an <see cref="Entity"/> that have a
/// representation in the world, like a <see cref="Unit"/> or a <see cref="Landscape"/>.
/// Those entity can block other units based on their tags.<br/>
/// An EntityPhysical will only block <see cref="EntityFilter.ETarget.Enemies"/>, on the square of the unit.<br/>
/// The visual representation isnt implemented here, but in each child classes.
/// </summary>
/// <remark>
/// An entity physical uses the filter feature of the <see cref="EntityFilter"/> as a hitbox
/// </remark>
public abstract class EntityPhysical : Entity {
    public enum ETeam {
        TeamA,
        TeamB,
        Neutral
    }

    private ETeam _team;    public ETeam GetTeam { get => _team; }
    private List<String> _tag = new List<String>(); public List<String> GetTagList { get => _tag; }

    // private Animation _animation; public Animation GetAnimation { get => _animation; }

    public EntityPhysical (EntityPhysical entityPhysical) : base (entityPhysical) {
        _team = entityPhysical._team;
        _tag = new List<string>(entityPhysical._tag);
    }

    public EntityPhysical (EntityPhysical entityPhysical, ETeam team) : base (entityPhysical) {
        _team = team;
        _tag = new List<string>(entityPhysical._tag);
    }

    public EntityPhysical (String title, String description, ETeam team, List<String> tag)
    : base (title, description) {
        _team = team;
        _tag = tag;
    }

    /// <summary>
    /// returns true if this entity is allied with ep
    /// </summary>
    public bool IsAliedTo (EntityPhysical ep) {
        return ep.GetTeam == _team;
    }

    /// <summary>
    /// returns true if this entity is allied with the player p
    /// </summary>
    public bool IsAliedTo (Player p) {
        return p.GetTeam == _team;
    }


    public override String ToString() {
        String ans = base.ToString() + '\n';
        ans += "Team: " +  _team + '\n';
        ans += "Tags:";
        foreach (String t in _tag) {
                ans += " " + t;
        }
        return ans;
    }
}
