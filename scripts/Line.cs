using Godot;
using System;

public abstract class Line : Node2D {
    public Tile Start = null;
    public Tile End = null;
    public bool Draw = false;

    public Line (Tile S, Tile E) {
        Start = S;
        End = E;
    }

    public Line () {
        Start = null;
        End = null;
    }
}