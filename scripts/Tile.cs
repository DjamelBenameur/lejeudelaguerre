using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// A Square represents a coordinate on the map, it can store a <see cref="Unit"/> and a <see cref="Landscape"/>, TODO
/// </summary>
public class Tile : Spatial {
	private Map _map;                   public Map SetMap { set => _map = value; }
	private int _x;                     public int X { get => _x;} public void SetX (int value){ _x = value;} 
	private int _y;                     public int Y { get => _y; } public void SetY (int value) { _y = value; }
	private Unit _localUnit = null;            public Unit GetLocalUnit { get => _localUnit; }
	private Terrain _localTerrain = null;      public Terrain GetLocalTerrain { get => _localTerrain; }
	
	private Renderer _effect;
	private static Vector3 _effectOffset = new Vector3(0, 0.01f, 0);
	private static List<Tile> _effectTiles = new List<Tile>(); public static List<Tile> GetEffectTiles { get => _effectTiles; }
	
	private static float _tileSize = 0.45f;
	private StaticBody _tileSelectionBody = new StaticBody();
	private StaticBody _POVRaycastingBody = new StaticBody();

	public enum EEffectType {
		Movement,
		Target,
		Selected,
		Shield,
		Range
	}

	public enum ERaycastLayer {
		TileSelection = 16,
		POVRaycasting = 32
	}

	public Tile (Tile tile, int x, int y) {
		_map = tile._map;
		_x = x;
		_y = y;
		_localUnit = null;
		_localTerrain = null;

		_effect = tile._effect.Clone("Movement");
		AddChild(_effect);
		_effect.Translation = _effectOffset;

		SetCollisionShape();
	}

	public Tile (int x, int y, String terrain, Renderer highlightRenderer) {
		_x = x;
		_y = y;
		_localTerrain = new Terrain(Terrain.GetTerrain(terrain));

		_effect = highlightRenderer;
		AddChild(_effect);
		_effect.Translation = _effectOffset;

		SetCollisionShape();
	}

	public Tile (int x, int y, Terrain localTerrain, Renderer highlightRenderer) {
		_x = x;
		_y = y;
		_localTerrain = localTerrain;
		_localUnit = null;

		_effect = highlightRenderer;
		AddChild(_effect);
		_effect.Translation = _effectOffset;

		SetCollisionShape();
	}

	public Tile (int x, int y, Renderer highlightRenderer) {
		_x = x;
		_y = y;

		_effect = highlightRenderer;
		AddChild(_effect);
		_effect.Translation = _effectOffset;

		SetCollisionShape();
	}

	private void SetCollisionShape () {
		//Tile selection collision shape
		CollisionShape tileSelectionCollisionShape = new CollisionShape();
		BoxShape tileSelectionShape = new BoxShape();

		tileSelectionShape.SetExtents(new Vector3(_tileSize, -0.001f, _tileSize));
		tileSelectionCollisionShape.SetShape(tileSelectionShape);

		_tileSelectionBody.AddChild(tileSelectionCollisionShape);
		tileSelectionCollisionShape.Translation = Vector3.Zero;

		AddChild(_tileSelectionBody);
		_tileSelectionBody.Translation = Vector3.Zero;
		_tileSelectionBody.CollisionLayer = (int)ERaycastLayer.TileSelection;

		//Action Raycasting collision shape
		CollisionShape POVRaycastingCollisionShape = new CollisionShape();
		BoxShape POVRaycastingShape = new BoxShape();

		POVRaycastingShape.SetExtents(new Vector3(_tileSize, _tileSize, _tileSize));
		POVRaycastingCollisionShape.SetShape(POVRaycastingShape);

		_POVRaycastingBody.AddChild(POVRaycastingCollisionShape);
		POVRaycastingCollisionShape.Translation = Vector3.Zero;

		AddChild(_POVRaycastingBody);
		_POVRaycastingBody.Translation = Vector3.Zero;
		_POVRaycastingBody.CollisionLayer = (int)ERaycastLayer.POVRaycasting;
	}

	public static void RemoveAllHighlightedTiles () {
		TextRenderer.ClearAll();
		foreach (Tile t in _effectTiles) {
			t._effect.Show(false);
		}
		_effectTiles.Clear();
	}

	public static void HighlightTiles (List<Tile> tiles, EEffectType effectType) {
		foreach (Tile t in tiles) {
			HighlightTile(t, effectType);
		}
	}

	public static void HighlightTiles (List<Unit> units, EEffectType effectType) {
		List<Tile> tiles = new List<Tile>();
		foreach (Unit u in units)
			tiles.Add(u.GetCurrentTile);
		HighlightTiles(tiles, effectType);
	}

	public static void HighlightTile (Tile t, EEffectType effectType) {
		t._effect.Show(true);
		if (!_effectTiles.Contains(t))
			_effectTiles.Add(t);
		
		switch (effectType) {
			case EEffectType.Movement:
				t._effect.PlayAnimation("Movement");
				break;
			case EEffectType.Target:
				t._effect.PlayAnimation("Target");
				break;
			case EEffectType.Selected:
				t._effect.PlayAnimation("Selected");
				break;
			case EEffectType.Shield:
				t._effect.PlayAnimation("Shield");
				break;
			case EEffectType.Range:
				t._effect.PlayAnimation("Range");
				break;
		}
	}

	public void SetLocalTerrain (Terrain terrain) {
		if (_localTerrain != null)
			return;
		_localTerrain = terrain;
		terrain.SetCurrentTile(this);
	}

	public void SetLocalUnit (Unit unit) {
		if (unit == null) {
			if (_localUnit != null) {
				RemoveChild(_localUnit);
			}
			_localUnit = null;
			return;
		}

		if (_localUnit != null)
			return;
		
		_localUnit = unit;
		AddChild(unit);
		unit.Translation = Vector3.Zero;
	}

	public void SetXY (int x, int y) {
		_x = x;
		_y = y;
	}
	public bool unitPresent () {
		return _localUnit != null;
	}

	public Tile getTile (int x, int y) {
		return _map.getTile(x, y);
	}
}
