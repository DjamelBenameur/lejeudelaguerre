using Godot;
using System;

public class CanvasGridDrawer : Node2D {
    private int _x;
    private int _y;
    private Camera _camera;
    private Color _color = new Color(.2f, .2f, .2f, .1f);
    private Vector2 _posA;
    private Vector2 _posB;
    private Vector3 _posA3D;
    private Vector3 _posB3D;

    public CanvasGridDrawer (int x, int y) {
        _camera = (Camera)Root.RootNode.GetNode("./CameraGroup/Camera");
        _x = x;
        _y = y;
    }

    public override void _Process(float delta) {
        Update();
    }

    //Draws a grid
    public override void _Draw() {
        for (int i = 0; i <= _x; i++) {
            _posA3D = new Vector3(.5f, 0, -i + .5f);
            _posB3D = new Vector3(-_y + .5f, 0, -i + .5f);

            if (!(_camera.IsPositionBehind(_posA3D) && _camera.IsPositionBehind(_posB3D))) {

                if (_camera.IsPositionBehind(_posA3D))
                    _posA3D = smartLerp(_posA3D, _posB3D);
                if (_camera.IsPositionBehind(_posB3D))
                    _posB3D = smartLerp(_posB3D, _posA3D);

                if (!(_camera.IsPositionBehind(_posA3D)) && !(_camera.IsPositionBehind(_posB3D))) {
                    _posA = _camera.UnprojectPosition(_posA3D);
                    _posB = _camera.UnprojectPosition(_posB3D);
                    DrawLine(_posA, _posB, _color, 1, true);
                }
            }
        }
        for (int i = 0; i <= _y; i++) {
            _posA3D = new Vector3(-i + .5f, 0, .5f);
            _posB3D = new Vector3(-i + .5f, 0, -_x + .5f);

            if (!(_camera.IsPositionBehind(_posA3D) && _camera.IsPositionBehind(_posB3D))) {
                
                if (_camera.IsPositionBehind(_posA3D))
                    _posA3D = smartLerp(_posA3D, _posB3D);
                if (_camera.IsPositionBehind(_posB3D))
                    _posB3D = smartLerp(_posB3D, _posA3D);

                if (!(_camera.IsPositionBehind(_posA3D)) && !(_camera.IsPositionBehind(_posB3D))) {
                    _posA = _camera.UnprojectPosition(_posA3D);
                    _posB = _camera.UnprojectPosition(_posB3D);
                    DrawLine(_posA, _posB, _color, 1, true);
                }
            }
        }
    }

    //Dichotomously tries to find a point that can be projected by the camera 
    private Vector3 smartLerp (Vector3 toMove, Vector3 reference) {
        Vector3 a = new Vector3(toMove);
        Vector3 b = new Vector3(reference);
        Vector3 m = new Vector3(Lerp(a, b, 0.5f));

        for (int i = 0; i < 6; i++) {
            if (_camera.IsPositionBehind(m)) {
                a = m;
            } else {
                b = m;
            }
            m = Lerp(a, b, 0.5f);
        }
        //In case the last iteration made m go back too much
        if (_camera.IsPositionBehind(m))
            m = b;
        return m;
    }

    private Vector3 Lerp (Vector3 a, Vector3 b, float t) {
        Vector3 v = new Vector3(a);
        v.x = Mathf.Lerp(a.x, b.x, t);
        v.y = Mathf.Lerp(a.y, b.y, t);
        v.z = Mathf.Lerp(a.z, b.z, t);
        return v;
    }
}