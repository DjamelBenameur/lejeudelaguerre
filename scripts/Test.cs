using Godot;
using System;

public class Test : Spatial {
    private PackedScene _unitScene;

    public override void _Ready()
    {
        CallDeferred("build");
    }

    private void build() {
        EntityBuilder b = new EntityBuilder("Kriegsspiel", this.GetTree().GetRoot());
		Console.WriteLine("Hello " + this.GetTree().GetRoot());
		if (b.GetUnitList != null){}
            foreach (Unit u in b.GetUnitList) {
                Console.WriteLine("\n\n");
                Console.WriteLine(u);
            }
    }
}
