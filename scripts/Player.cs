using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// player class, it processes player inputs as moving units, attacking...
/// </summary>
/// <param name="playerState"> enumerate of different player states </param>
/// <param name="selectedUnit"> the unit selected by the player </param>
/// <param name="team"> the team for which the player belongs </param>
/// <param name="unitList"> list of unit which contains the various element of the team  </param>
/// <param name="toPlaceUnitList"> the current unit to place </param>
/// <param name="fleeingUnit"> the unit which must flee the enemy  </param>
/// <param name="movedUnits"> list of units to move  </param>
/// <param name="hoveringTile"> list of game squares to fly over  </param>
public class Player : Node {
	public enum EPlayerState {
		Initialising = 0,
		Fleeing = 1,
		Moving = 2,
		Attacking = 3,
		Waiting = 4
	}

	private EPlayerState _playerState = EPlayerState.Waiting;
	private Unit _selectedUnit;
	private EntityPhysical.ETeam _team = EntityPhysical.ETeam.TeamA; public EntityPhysical.ETeam GetTeam { get => _team; }
	private List<Unit> _unitList = new List<Unit>(); public List<Unit> GetUnitList { get => _unitList; }
	private List<Unit> _toPlaceUnitList;
	private Unit _fleeingUnit = null; public Unit SetFleeingUnit { set => _fleeingUnit = value; }
	private List<Unit> _movedUnits = new List<Unit>();
	private Tile _hoveringTile = null;
	private bool _initialized = false; public bool Initialized { get => _initialized; }
	private List<Unit> _attackList = new List<Unit>();
	private Unit _selectedForAttackUnit;
	private String _teamName;
	public static List<Player> _playerList = new List<Player>(); public static List<Player> GetPlayerList { get => _playerList; }
	public List<Passive> TeamPassiveList {get;} = new List<Passive>();

	public Player (int team, List<Unit> unitList, String teamName) {
		foreach (Unit u in unitList) {
			u.Skin(team);
			
		}

		_toPlaceUnitList = unitList;

		_team = (EntityPhysical.ETeam)team; ///Caution, this value should be the same as the units, check the unit copy constructor 
		_teamName = teamName;


		_playerList.Add(this);
	}

	public void OnUnitKilled (Unit u) {
		_unitList.Remove(u);
		_attackList.Remove(u);
		_toPlaceUnitList.Remove(u);
		_movedUnits.Remove(u);
		foreach (Passive a in u.GetPassiveList) {
			TeamPassiveList.Remove(a);
		}
	}

	/// <summary>
	/// Processes the input of the player, when we click on a tile, we call Click(t), when we hover a tile, we call Hover(t)
	/// </summary>
	public override void _Input(InputEvent @event) {
		//CLICK PART
		if (@event is InputEventMouseButton) {
			InputEventMouseButton emb = (InputEventMouseButton)@event;
			if (emb.ButtonIndex == (int)ButtonList.Left && emb.Pressed) {
				Camera camera = GetViewport().GetCamera();
				Vector3 from = camera.ProjectRayOrigin(emb.Position);
				Vector3 to = from + camera.ProjectRayNormal(emb.Position) * 1000;

				PhysicsDirectSpaceState spaceState = camera.GetWorld().DirectSpaceState;
				Godot.Collections.Dictionary result = spaceState.IntersectRay(from, to, null, (int)Tile.ERaycastLayer.TileSelection);

				if (result.Count > 0) {
					Node n = (Node)result["collider"];
					Tile t = (Tile)n.GetParent();
					Click(t);
				}
			}
		}
		//HOVER PART
		if (@event is InputEventMouseMotion) {
			InputEventMouseMotion emm = (InputEventMouseMotion)@event;
			Camera camera = GetViewport().GetCamera();
				Vector3 from = camera.ProjectRayOrigin(emm.Position);
				Vector3 to = from + camera.ProjectRayNormal(emm.Position) * 1000;

				PhysicsDirectSpaceState spaceState = camera.GetWorld().DirectSpaceState;
				Godot.Collections.Dictionary result = spaceState.IntersectRay(from, to, null, (int)Tile.ERaycastLayer.TileSelection);

				if (result.Count > 0) {
					Node n = (Node)result["collider"];
					Tile t = (Tile)n.GetParent();
					Hover(t);
				}
		}
	}

	/// <summary>
	/// Shows information based on the tile hovered, and the current phase of the game.
	/// </summary>
	public void Hover (Tile t) {
		//if we are on our moving phase, we show the helping and attacking range of the hovered units
		if (_playerState == EPlayerState.Moving && _selectedUnit == null) {
			if (t != _hoveringTile) {
				_hoveringTile = t;
				Tile.RemoveAllHighlightedTiles();
				
				if (t.GetLocalUnit != null) {
					t.GetLocalUnit.TileRange();
					t.GetLocalUnit.AttackRange();
					t.GetLocalUnit.HelpRange();
				}
			}
		}
	}

	/// <summary>
	/// Calls actions based on the phase of the game (_playerState)
	/// </summary>
	public void Click (Tile t) {
		switch (_playerState)
		{
			case EPlayerState.Fleeing:
				FleeClick(t);
				break;
			case EPlayerState.Moving:
				MoveClick(t);
				break;
			case EPlayerState.Attacking:
				AttackClick(t);
				break;
			case EPlayerState.Waiting:
				break;
			case EPlayerState.Initialising:
				InitialiseClick(t);
				break;
		}
	}

	/// <summary>
	/// Click used on the fleeing phase
	/// </summary>
	public void FleeClick (Tile t) { 
		if (_fleeingUnit == null) {
			return;
		}

		//TODO: if the fleeing unit can move, we ask for a movement, if it cannot, we kill it
		if (!_fleeingUnit.Move(t)) {
			_fleeingUnit.MovingRange();
		} else {
			_fleeingUnit = null;
		}
	}

	/// <summary>
	/// Click used on the movement phase
	/// </summary>
	public void MoveClick (Tile t) {
		//If the unit we clicked is our unit, and if we still have movements to do, we select the unit
		if (t.GetLocalUnit != null && t.GetLocalUnit.GetTeam == _team && !t.GetLocalUnit.MovedThisTurn && _movedUnits.Count < 5) { //TODO, dynamic limit to movements (map config file)
			t.GetLocalUnit.MovingRange();
			_selectedUnit = t.GetLocalUnit;
		} else {
			//If we selected a unit previously
			if (_selectedUnit != null) {
				if (_selectedUnit.Move(t)) {
					//If the selected unit moved, we add it to the moved unit array
					_movedUnits.Add(_selectedUnit);        
				}
			}
			_selectedUnit = null;
		}
	}

	/// <summary>
	/// Click used on the attack phase
	/// </summary>
	public void AttackClick (Tile t) {
		if (t.GetLocalUnit != null && _attackList.Contains(t.GetLocalUnit) && _selectedForAttackUnit == null) {
			//We can attack this tile... we store the selected unit and highlight stuffs
			Tile.RemoveAllHighlightedTiles();
			_selectedForAttackUnit = t.GetLocalUnit;
			//Tile.HighlightTile(t, Tile.EEffectType.Shield);
			_selectedForAttackUnit.AttackedByRange();
			_selectedForAttackUnit.HelpedByRange();
			Tile.HighlightTile(_selectedForAttackUnit.GetCurrentTile, Tile.EEffectType.Target);
			_selectedForAttackUnit.GetTextRenderer.Print(""+EvaluateHit(_selectedForAttackUnit), TextRenderer.ETextRenderType.Danger);
			_selectedForAttackUnit.GetTextRenderer.Print(""+EvaluateHelp(_selectedForAttackUnit), TextRenderer.ETextRenderType.Support);
		} else {
			//We clicked on a tile that cannot be targeted...
			if (_selectedForAttackUnit != null) {
				Tile.RemoveAllHighlightedTiles();
				Tile.HighlightTiles(_attackList, Tile.EEffectType.Target);
			}
			_selectedForAttackUnit = null;
		}
	}

	/// <summary>
	/// Click used on the initialisation phase
	/// </summary>
	public void InitialiseClick (Tile t) {
		//TODO: better way of placing the units on the board... maybe undoing, click and drag...
		if (t.GetLocalUnit != null)
			return;
		if (_toPlaceUnitList.Count == 0)
			return;

		if (_toPlaceUnitList[0].SetCurrentTile(t)) {
			_unitList.Add(_toPlaceUnitList[0]);
			_toPlaceUnitList.RemoveAt(0);
		}
	}

	/// <summary>
	/// Return true if the player is on the waiting phase
	/// </summary>
	public bool IsWaiting () {
		return _playerState == EPlayerState.Waiting;
	}

	/// <summary>
	/// increments the state of the player (phase). fleeing -> moving -> attack -> waiting. returns true if the player is now waiting (meaning that it is now to another player to play)
	/// </summary>
	public bool NextState () {
		Tile.RemoveAllHighlightedTiles();

		if (_toPlaceUnitList.Count != 0) { //Initialisation
			_playerState = EPlayerState.Initialising;
			return false;
		}

		if (_playerState == EPlayerState.Initialising) { //Initialisation ended
			_playerState = EPlayerState.Waiting;
			_initialized = true;
			//We fill & sort the passive list based on priority
			foreach(Unit u in _unitList) {
				foreach (Passive p in u.GetPassiveList) {
					TeamPassiveList.Add(p);
				}
			}
			TeamPassiveList.Sort((y, x)=>x.Priority.CompareTo(y.Priority));
			return true;
		}

		_playerState++;

		if (_playerState >= EPlayerState.Waiting) { //Now waiting
			//Here we evaluate the attacks
			if (_selectedForAttackUnit != null) {
				int result = EvaluateAllAttacks(_selectedForAttackUnit);
				
				if (result > 0) {
					_selectedForAttackUnit.Kill();
				} else if (result == 0) {
					_selectedForAttackUnit.Flee();
				}
			}
			_playerState = EPlayerState.Waiting;
			return true;
		}

		if (_playerState == EPlayerState.Attacking) {
			foreach (Unit u in _unitList) {
				List<Unit> tmpList = u.AttackRange();
				foreach (Unit tmpu in tmpList) {
					if (!tmpu.IsAliedTo(this)) {
						if (!_attackList.Contains(tmpu)) {
							_attackList.Add(tmpu);
							Tile.HighlightTile(tmpu.GetCurrentTile, Tile.EEffectType.Target);
						}
					}
				}
			}
			Tile.RemoveAllHighlightedTiles();
			Tile.HighlightTiles(_attackList, Tile.EEffectType.Target);
		}

		return false;
	}

	public int EvaluateAllAttacks (Unit tgt) {
		int result = 0;
		foreach(Unit u in Unit.GetUnitBoardList) {
					result += u.EvaluateAttacksOn(tgt);
		}
		return result;
	}

	public int EvaluateHelp (Unit tgt) {
		int result = 0;
		foreach(Unit u in Unit.GetUnitBoardList) {
					int tmp = u.EvaluateAttacksOn(tgt);
					if (tmp < 0) {
						result -= tmp;
					}
		}
		return result;
	}

	public int EvaluateHit (Unit tgt) {
		int result = 0;
		foreach(Unit u in Unit.GetUnitBoardList) {
					int tmp = u.EvaluateAttacksOn(tgt);
					if (tmp > 0) {
						result += tmp;
					}
		}
		return result;
	}
	
	
	/// <summary>
	/// Used to setup a player for a new turn
	/// </summary>
	public void NewTurn () {
		Tile.RemoveAllHighlightedTiles();

		if (_toPlaceUnitList.Count != 0) { //If we have units to place, set the phase to initialisation
			_playerState = EPlayerState.Initialising;
		} else {
			//We set the phase to fleeing
			_playerState = EPlayerState.Fleeing;
			if (_fleeingUnit == null) { //If we dont have any unit that needs to flee, we jump to moving
				_playerState = EPlayerState.Moving;
			} else {
				if (_fleeingUnit.MovingRange().Count == 0) { //If we have an unit that needs to flee, but that cannot move, we kill it and jump to moving
					_fleeingUnit.Kill();
					_fleeingUnit = null;
					_playerState = EPlayerState.Moving;
				}
			}
		}

		//We clear the attack data from the previous turn
		_attackList.Clear();
		_selectedForAttackUnit = null;

		//We clear the moved units from the previous turn
		foreach (Unit u in _movedUnits) {
			u.MovedThisTurn = false;
		}
		_movedUnits.Clear();

		ResetUnitsDatasheets(); //reset the buffs etc
		EvaluatePassives(); //We evaluate all of the team's passives
	}

	private void ResetUnitsDatasheets () {
		foreach (Player p in _playerList) {
			foreach (Unit u in p.GetUnitList) {
				u.GetDataSheet.reset();
			}
		}
	}

	private void EvaluatePassives () {
		foreach (Player p in _playerList) {
			foreach (Passive a in p.TeamPassiveList) {
				a.Init();	
			}
			foreach (Passive a in p.TeamPassiveList) {
				a.Do();	
			}
		}
	}

	public String InfoStringLeft () {
		String ans = (int)_team + "\n";
		ans += _playerState;
		return ans;
	}

}
