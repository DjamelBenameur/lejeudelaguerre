using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// An EntityBuilder builds all of the entities based on the selected addon, and parents them to the current scene
/// </summary>
public class EntityBuilder : Node {
	private String _addon; public String GetAddon { get=> _addon; }
	private String _addonPath; public String GetAddonPath { get=> _addonPath; }
	private List<Unit> _unitList; public List<Unit> GetUnitList { get=> _unitList; }
	private List<Unit> _teamB; public List<Unit> GetTeamB { get => _teamB; }
	private static Node _sceneNode; public static Node GetSceneNode { get=> _sceneNode; }
	private List<Map> _listMaps; public List<Map> GetListMaps () { return  _listMaps; }
	private Tile _defaultTile = null; public Tile GetDefaultTile { get => _defaultTile; }
	private List<Map> _mapList = new List<Map>(); public List<Map> GetMapList { get => _mapList; }
	private TextRenderer _defaultTextRenderer;

	//Renderers
	String _RendererStr = "SpriteRenderer";

	/// <summary>
	/// Instantiate an EntityBuilder that will build the scene tree
	/// </summary>
	/// <param name="addon"> the directory that will be selected by the EntityBuilder </param>
	/// <param name="sceneNode"> will be considered the root of the scene </param>
	public EntityBuilder (String addon, Node sceneNode) {
		_addon = addon;
		_sceneNode = sceneNode;
		// _sceneNode.AddChild(this);
		String addonPath = System.IO.Path.Combine(new String[] {"Build", "Addons", _addon});

		_addonPath = addonPath;

		Console.WriteLine("*************BUILDING TEXTRENDERER...");
		_defaultTextRenderer = buildDefaultTextRenderer(addonPath);
		Console.WriteLine("*************BUILDING ATTACKS...");
		buildAttacks(addonPath);
		Console.WriteLine("*************BUILDING PASSIVES...");
		buildPassives(addonPath);
		Console.WriteLine("*************BUILDING TERRAINS...");
		buildTerrains(addonPath);
		Console.WriteLine("*************BUILDING UNITS...");
		_unitList = buildUnits(addonPath);
		Console.WriteLine("*************BUILDING TILE...");
		_defaultTile = buildDefaultTile(addonPath);
		Console.WriteLine("*************BUILDING MAPS...");
		buildMaps(addonPath);
		//_teamB = buildUnits(EntityPhysical.ETeam.TeamB);
	}



	/// <summary>
	/// Creates a new node, Build and set as child all of the units of the node
	/// </summary>
	/// <param name="team"> the team ( <see cref="EntityPhysical.ETeam"/>) to build</param>
	private List<Unit> buildUnits (String path) {
		//Here we store the units directories
		String[] unitDir = System.IO.Directory.GetDirectories(System.IO.Path.Combine(path, "Units"));

		//For each unit directory, we build an unit
		List<Unit> unitObj = new List<Unit>();
		Console.WriteLine("Nombre junit = " + unitDir.Length);
		for (int i = 0; i < unitDir.Length; i++) {
			Unit tmp = buildSingleUnit(unitDir[i]);
			if (tmp != null) {
				unitObj.Add(tmp);
			} else {
				Console.WriteLine("An error occured while creating a unit");
			}
		}
		return unitObj;
	}



	/// <summary>
	/// Creates all of the attacks in the "attacks" directory
	/// </summary>
	/// <param name="path"> the path of the addon</param>
	private void buildAttacks (String path) {

		//Here we store the attack files
		String[] attackDir = System.IO.Directory.GetFiles(System.IO.Path.Combine(path, "Attacks"));

		//For each attack file, we build an attack
		for (int i = 0; i < attackDir.Length; i++) {
			buildSingleAttack(attackDir[i]);
		}
	}



	/// <summary>
	/// Creates all of the passives in the "passives" directory
	/// </summary>
	/// <param name="path"> the path of the addon</param>
	private void buildPassives (String path) {

		//Here we store the attack files
		String[] passiveDir = System.IO.Directory.GetFiles(System.IO.Path.Combine(path, "Passives"));

		//For each attack file, we build an attack
		for (int i = 0; i < passiveDir.Length; i++) {
			buildSinglePassive(passiveDir[i]);
		}
	}



	/// <summary>
	/// Creates all of the terrains in the "terrains" directory
	/// </summary>
	/// <param name="path"> the path of the addon</param>
	private void buildTerrains (String path) {

		//Here we store the attack files
		String[] passiveDir = System.IO.Directory.GetDirectories(System.IO.Path.Combine(path, "Terrains"));

		//For each attack file, we build an attack
		for (int i = 0; i < passiveDir.Length; i++) {
			buildSingleTerrain(passiveDir[i]);
		}
	}


	private void buildMaps (String path) {

		String[] mapFiles = System.IO.Directory.GetFiles(System.IO.Path.Combine(path, "Maps"));

		for (int i = 0; i < mapFiles.Length; i++) {
			Map tmp = buildSingleMap(mapFiles[i]);
			if (tmp != null)
				_mapList.Add(tmp);
				//tmp.SetListUnits(_unitList);
		}
	}


	/// <summary>
	/// Instantiates the default tile, that will be cloned for each tile of the map
	/// </summary>
	private Tile buildDefaultTile (String path) {
		Console.Write("    Building tile... ");
		path = System.IO.Path.Combine(path, "Tile");
		if (!System.IO.File.Exists(System.IO.Path.Combine(path, "tile.conf"))) {
			System.Console.WriteLine("Error: Couldnt find tile.conf@" + path);
			return null;
		}
		if (!System.IO.File.Exists(System.IO.Path.Combine(path, "Movement.png"))) {
			System.Console.WriteLine("Error: Couldnt find Movement.png@" + path);
			return null;
		}
		if (!System.IO.File.Exists(System.IO.Path.Combine(path, "Target.png"))) {
			System.Console.WriteLine("Error: Couldnt find Target.png@" + path);
			return null;
		}
		if (!System.IO.File.Exists(System.IO.Path.Combine(path, "Range.png"))) {
			System.Console.WriteLine("Error: Couldnt find Range.png@" + path);
			return null;
		}
		if (!System.IO.File.Exists(System.IO.Path.Combine(path, "Selected.png"))) {
			System.Console.WriteLine("Error: Couldnt find Selected.png@" + path);
			return null;
		}
		if (!System.IO.File.Exists(System.IO.Path.Combine(path, "Shield.png"))) {
			System.Console.WriteLine("Error: Couldnt find Shield.png@" + path);
			return null;
		}

		String dirPath = path;
		path = System.IO.Path.Combine(path, "tile.conf");

		//This will be used to build the tile
		Renderer renderer = new SpriteRenderer(); //Used for the effects

		//We open the config file
		File configFile = new File();
		configFile.Open(path, File.ModeFlags.Read);
		JSONParseResult parseResult = JSON.Parse(configFile.GetAsText());
		if (parseResult.Error != Error.Ok) {
			System.Console.WriteLine("\n" + path);
			System.Console.WriteLine(parseResult.ErrorString);
			return null;
		}

		///DATAS
		//Root
		if (!(parseResult.Result is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : root@" + path);
			return null;
		}
		Godot.Collections.Dictionary dictionary = (Godot.Collections.Dictionary)parseResult.Result;

		//Tile
		if (!dictionary.Contains("tile") || !(dictionary["tile"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : tile@" + path);
			return null;
		}
		Godot.Collections.Dictionary tileDict = (Godot.Collections.Dictionary)dictionary["tile"];

		//Tile.Movement
		if (!tileDict.Contains("movement") || !(tileDict["movement"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : tile.movement@" + path);
			return null;
		}
		Godot.Collections.Dictionary movementDict = (Godot.Collections.Dictionary)tileDict["movement"];
		if (!renderer.Config(movementDict, dirPath, "Movement")) {
			return null;
		}

		//Tile.Target
		if (!tileDict.Contains("target") || !(tileDict["target"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : tile.target@" + path);
			return null;
		}
		Godot.Collections.Dictionary targetDict = (Godot.Collections.Dictionary)tileDict["target"];
		if (!renderer.Config(targetDict, dirPath, "Target")) {
			return null;
		}

		//Tile.Selected
		if (!tileDict.Contains("selected") || !(tileDict["selected"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : tile.selected@" + path);
			return null;
		}
		Godot.Collections.Dictionary selectedDict = (Godot.Collections.Dictionary)tileDict["selected"];
		if (!renderer.Config(selectedDict, dirPath, "Selected")) {
			return null;
		}

		//Tile.Range
		if (!tileDict.Contains("range") || !(tileDict["range"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : tile.range@" + path);
			return null;
		}
		Godot.Collections.Dictionary rangeDict = (Godot.Collections.Dictionary)tileDict["range"];
		if (!renderer.Config(rangeDict, dirPath, "Range")) {
			return null;
		}

		//Tile.Shield
		if (!tileDict.Contains("shield") || !(tileDict["shield"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : tile.shield@" + path);
			return null;
		}
		Godot.Collections.Dictionary shieldDict = (Godot.Collections.Dictionary)tileDict["shield"];
		if (!renderer.Config(shieldDict, dirPath, "Shield")) {
			return null;
		}


		Console.WriteLine("Ok !");
		return new Tile(0, 0, renderer);
	}

	/// <summary>
	/// Instantiates the default text renderer, it will be copied for each unit of the map
	/// </summary>
	private TextRenderer buildDefaultTextRenderer (String path) {
		Console.Write("    Building text renderer... ");
		path = System.IO.Path.Combine(path, "TextRenderer");
		if (!System.IO.File.Exists(System.IO.Path.Combine(path, "TextRenderer.conf"))) {
			System.Console.WriteLine("Error: Couldnt find TextRenderer.conf @ " + path);
			return null;
		}

		String dirPath = path;
		path = System.IO.Path.Combine(path, "TextRenderer.conf");

		TextRenderer textRenderer = new RichTextRenderer(_sceneNode);

		//We open the config file
		File configFile = new File();
		configFile.Open(path, File.ModeFlags.Read);
		JSONParseResult parseResult = JSON.Parse(configFile.GetAsText());
		if (parseResult.Error != Error.Ok) {
			System.Console.WriteLine("\n" + path);
			System.Console.WriteLine(parseResult.ErrorString);
			return null;
		}

		///DATAS
		//Root
		if (!(parseResult.Result is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : root@" + path);
			return null;
		}
		Godot.Collections.Dictionary dictionary = (Godot.Collections.Dictionary)parseResult.Result;

		//TextRenderer
		if (!dictionary.Contains("textrenderer") || !(dictionary["textrenderer"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : textrenderer@" + path);
			return null;
		}
		Godot.Collections.Dictionary textRendererDict = (Godot.Collections.Dictionary)dictionary["textrenderer"];
		if (!textRenderer.Config(textRendererDict)) {
			return null;
		}

		Console.WriteLine("Ok !");
		return textRenderer;
	}


	/// <summary>
	/// Called by BuildAttacks
	/// </summary>
	private void buildSingleAttack(String path) {
		Console.Write("    Building attack... ");
		if (!System.IO.File.Exists(path)) {
			return;
		}
		if (!System.IO.Path.GetExtension(path).Equals(".conf")) {
			return;
		}

		//This will be used to build the attack
		String title = null;
		String description = null;
		List<String> filter = new List<String>();
		Action.ETarget filterIgnore = (Action.ETarget)(-1);
		Shape shape = null;
		List<String> blockedBy = new List<String>();
		Action.ETarget blockedByIgnore = (Action.ETarget)(-1);
		Unit parentUnit = null;
		int damage = -1;

		//We open the config file
		File configFile = new File();
		configFile.Open(path, File.ModeFlags.Read);
		JSONParseResult parseResult = JSON.Parse(configFile.GetAsText());
		if (parseResult.Error != Error.Ok) {
			System.Console.WriteLine("\n" + path);
			System.Console.WriteLine(parseResult.ErrorString);
			return;
		}

		///DATAS
		//Root
		if (!(parseResult.Result is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : root@" + path);
			return;
		}
		Godot.Collections.Dictionary dictionary = (Godot.Collections.Dictionary)parseResult.Result;

		//Attack
		if (!dictionary.Contains("attack") || !(dictionary["attack"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : attack@" + path);
			return;
		}
		Godot.Collections.Dictionary attackDict = (Godot.Collections.Dictionary)dictionary["attack"];

		//Attack.Name
		if (!attackDict.Contains("name") || !(attackDict["name"] is String)) {
			System.Console.WriteLine("Error : attack.name@" + path);
			return;
		}
		title = (String)attackDict["name"];

		//Attack.Description
		if (!attackDict.Contains("description") || !(attackDict["description"] is String)) {
			System.Console.WriteLine("Error : attack.description@" + path);
			return;
		}
		description = (String)attackDict["description"];

		//Attack.Damage
		if (!attackDict.Contains("damage") || !(attackDict["damage"] is float)) {
			System.Console.WriteLine("Error : attack.damage@" + path);
			return;
		}
		damage = (int)(float)attackDict["damage"];

		//Attack.Filter
		if (!attackDict.Contains("filter") || !(attackDict["filter"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : attack.filter@" + path);
			return;
		}
		Godot.Collections.Dictionary filterDict = (Godot.Collections.Dictionary)attackDict["filter"];

		//Attack.Filter.tags
		if (!filterDict.Contains("tags") || !(filterDict["tags"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : attack.filter.tags@" + path);
			return;
		}
		Godot.Collections.Array tags = (Godot.Collections.Array)filterDict["tags"];
		foreach (System.Object o in tags) {
			if (!(o is String)) {
				System.Console.WriteLine("Error : attack.filter.tags.element@" + path);
				return;
			}
			filter.Add((String)o);
		}

		//Attack.Filter.Ignore
		if (!filterDict.Contains("ignore") || !(filterDict["ignore"] is String)) {
			System.Console.WriteLine("Error : attack.filter.ignore@" + path);
			return;
		}
		String ignore = (String)filterDict["ignore"];
		filterIgnore = Action.StrToETarget(ignore);
		if ((int)filterIgnore == -1) {
			System.Console.WriteLine("Error : attack.filter.ignore.value@" + path);
			return;
		}

		//Attack.BlockedBy
		if (!attackDict.Contains("blockedby") || !(attackDict["blockedby"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : attack.blockedby@" + path);
			return;
		}
		Godot.Collections.Dictionary blockedByDict = (Godot.Collections.Dictionary)attackDict["blockedby"];

		//Attack.BlockedBy.Tags
		if (!blockedByDict.Contains("tags") || !(blockedByDict["tags"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : attack.blockedby.tags@" + path);
			return;
		}
		tags = (Godot.Collections.Array)blockedByDict["tags"];
		foreach (System.Object o in tags) {
			if (!(o is String)) {
				System.Console.WriteLine("Error : attack.blockedby.tags.element@" + path);
				return;
			}
			blockedBy.Add((String)o);
		}

		//Attack.BlockedBy.Ignore
		if (!blockedByDict.Contains("ignore") || !(blockedByDict["ignore"] is String)) {
			System.Console.WriteLine("Error : attack.blockedby.ignore@" + path);
			return;
		}
		ignore = (String)blockedByDict["ignore"];
		blockedByIgnore = Action.StrToETarget(ignore);
		if ((int)blockedByIgnore == -1) {
			System.Console.WriteLine("Error : attack.blockedby.ignore.value@" + path);
			return;
		}

		//Attack.Shape
		if (!attackDict.Contains("shape") || !(attackDict["shape"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : attack.shape@" + path);
			return;
		}
		Godot.Collections.Dictionary shapeDict = (Godot.Collections.Dictionary)attackDict["shape"];

		//Attack.Shape.Vertical
		if (!shapeDict.Contains("vertical") || !(shapeDict["vertical"] is bool)) {
			System.Console.WriteLine("Error : attack.shape.vertical@" + path);
			return;
		}
		bool shapeVertical = (bool)shapeDict["vertical"];

		//Attack.Shape.Diagonal
		if (!shapeDict.Contains("diagonal") || !(shapeDict["diagonal"] is bool)) {
			System.Console.WriteLine("Error : attack.shape.diagonal@" + path);
			return;
		}
		bool shapeDiagonal = (bool)shapeDict["diagonal"];

		//Attack.Shape.Range
		if (!shapeDict.Contains("range") || !(shapeDict["range"] is float)) {
			System.Console.WriteLine("Error : attack.shape.range@" + path);
			return;
		}
		int shapeRange = (int)(float)shapeDict["range"];


		//Attack.Shape.MinRange
		if (!shapeDict.Contains("min_range") || !(shapeDict["min_range"] is float)) {
			System.Console.WriteLine("Error : attack.shape.min_range@" + path);
			return;
		}
		int shapeMinRange = (int)(float)shapeDict["min_range"];

		//Attack.Shape.MaxRange
		if (!shapeDict.Contains("max_range") || !(shapeDict["max_range"] is float)) {
			System.Console.WriteLine("Error : attack.shape.max_range@" + path);
			return;
		}
		int shapeMaxRange = (int)(float)shapeDict["max_range"];

		//title check
		Attack a = Attack.GetAttack(title);
		if (a != null) {
			System.Console.WriteLine("Error : name_already_taken@" + path);
			return;
		}

		shape = new Shape(shapeVertical, shapeDiagonal, shapeRange, shapeMinRange, shapeMaxRange);
		new Attack(title, description, filter, shape, filterIgnore, blockedBy, blockedByIgnore, parentUnit, damage);

		System.Console.WriteLine("OK !");
	}

	/// <summary>
	/// Called by BuildPassives
	/// </summary>
	private void buildSinglePassive(String path) {
		Console.Write("    Building passive... ");
		if (!System.IO.File.Exists(path)) {
			System.Console.WriteLine("Error : File doesn't exist @" + path);
			return;
		}
		if (!System.IO.Path.GetExtension(path).Equals(".conf")) {
			System.Console.WriteLine("Error : @" + path);
			return;
		}

		String title = null;
		String description = null;
		List<String> filter = new List<String>();
		Action.ETarget filterIgnore = (Action.ETarget)(-1);
		Shape shape = null;
		List<String> blockedBy = new List<String>();
		Action.ETarget blockedByIgnore = (Action.ETarget)(-1);
		Passive passive = null;

		//We open the config file
		File configFile = new File();
		configFile.Open(path, File.ModeFlags.Read);
		JSONParseResult parseResult = JSON.Parse(configFile.GetAsText());
		if (parseResult.Error != Error.Ok) {
			System.Console.WriteLine("\n" + path);
			System.Console.WriteLine(parseResult.ErrorString);
			return;
		}

		///DATAS
		//Root
		if (!(parseResult.Result is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : root@" + path);
			return;
		}
		Godot.Collections.Dictionary dictionary = (Godot.Collections.Dictionary)parseResult.Result;

		//Passive
		if (!dictionary.Contains("passive") || !(dictionary["passive"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : passive@" + path);
			return;
		}
		Godot.Collections.Dictionary passiveDict = (Godot.Collections.Dictionary)dictionary["passive"];

		//Passive.Name
		if (!passiveDict.Contains("name") || !(passiveDict["name"] is String)) {
			System.Console.WriteLine("Error : passive.name@" + path);
			return;
		}
		title = (String)passiveDict["name"];

		//Passive.Description
		if (!passiveDict.Contains("description") || !(passiveDict["description"] is String)) {
			System.Console.WriteLine("Error : passive.description@" + path);
			return;
		}
		description = (String)passiveDict["description"];

		//Passive.Filter
		if (!passiveDict.Contains("filter") || !(passiveDict["filter"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : passive.filter@" + path);
			return;
		}
		Godot.Collections.Dictionary filterDict = (Godot.Collections.Dictionary)passiveDict["filter"];

		//Passive.Filter.tags
		if (!filterDict.Contains("tags") || !(filterDict["tags"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : passive.filter.tags@" + path);
			return;
		}
		Godot.Collections.Array tags = (Godot.Collections.Array)filterDict["tags"];
		foreach (System.Object o in tags) {
			if (!(o is String)) {
				System.Console.WriteLine("Error : passive.filter.tags.element@" + path);
				return;
			}
			filter.Add((String)o);
		}

		//Passive.Filter.Ignore
		if (!filterDict.Contains("ignore") || !(filterDict["ignore"] is String)) {
			System.Console.WriteLine("Error : passive.filter.ignore@" + path);
			return;
		}
		String ignore = (String)filterDict["ignore"];
		filterIgnore = Action.StrToETarget(ignore);
		if ((int)filterIgnore == -1) {
			System.Console.WriteLine("Error : passive.filter.ignore.value@" + path);
			return;
		}

		//Passive.BlockedBy
		if (!passiveDict.Contains("blockedby") || !(passiveDict["blockedby"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : passive.blockedby@" + path);
			return;
		}
		Godot.Collections.Dictionary blockedByDict = (Godot.Collections.Dictionary)passiveDict["blockedby"];

		//Passive.BlockedBy.Tags
		if (!blockedByDict.Contains("tags") || !(blockedByDict["tags"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : passive.blockedby.tags@" + path);
			return;
		}
		tags = (Godot.Collections.Array)blockedByDict["tags"];
		foreach (System.Object o in tags) {
			if (!(o is String)) {
				System.Console.WriteLine("Error : passive.blockedby.tags.element@" + path);
				return;
			}
			blockedBy.Add((String)o);
		}

		//Passive.BlockedBy.Ignore
		if (!blockedByDict.Contains("ignore") || !(blockedByDict["ignore"] is String)) {
			System.Console.WriteLine("Error : passive.blockedby.ignore@" + path);
			return;
		}
		ignore = (String)blockedByDict["ignore"];
		blockedByIgnore = Action.StrToETarget(ignore);
		if ((int)blockedByIgnore == -1) {
			System.Console.WriteLine("Error : passive.blockedby.ignore.value@" + path);
			return;
		}

		//Passive.Shape
		if (!passiveDict.Contains("shape") || !(passiveDict["shape"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : passive.shape@" + path);
			return;
		}
		Godot.Collections.Dictionary shapeDict = (Godot.Collections.Dictionary)passiveDict["shape"];

		//Passive.Shape.Vertical
		if (!shapeDict.Contains("vertical") || !(shapeDict["vertical"] is bool)) {
			System.Console.WriteLine("Error : passive.shape.vertical@" + path);
			return;
		}
		bool shapeVertical = (bool)shapeDict["vertical"];

		//Passive.Shape.Diagonal
		if (!shapeDict.Contains("diagonal") || !(shapeDict["diagonal"] is bool)) {
			System.Console.WriteLine("Error : passive.shape.diagonal@" + path);
			return;
		}
		bool shapeDiagonal = (bool)shapeDict["diagonal"];

		//Passive.Shape.Range
		if (!shapeDict.Contains("range") || !(shapeDict["range"] is float)) {
			System.Console.WriteLine("Error : passive.shape.range@" + path);
			return;
		}
		int shapeRange = (int)(float)shapeDict["range"];

		//Passive.Shape.MinRange
		if (!shapeDict.Contains("min_range") || !(shapeDict["min_range"] is float)) {
			System.Console.WriteLine("Error : passive.shape.min_range@" + path);
			return;
		}
		int shapeMinRange = (int)(float)shapeDict["min_range"];

		//Passive.Shape.MaxRange
		if (!shapeDict.Contains("max_range") || !(shapeDict["max_range"] is float)) {
			System.Console.WriteLine("Error : passive.shape.max_range@" + path);
			return;
		}
		int shapeMaxRange = (int)(float)shapeDict["max_range"];

		//title check
		Passive a = Passive.GetPassive(title);
		if (a != null) {
			System.Console.WriteLine("Error : name_already_taken@" + path);
			return;
		}

		//Passive.PassiveData
		if (!passiveDict.Contains("passivedata") || !(passiveDict["passivedata"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : passive.passivedata@" + path);
			return;
		}
		Godot.Collections.Dictionary passiveDataDict = (Godot.Collections.Dictionary)passiveDict["passivedata"];

		//Passive.PassiveData.Type
		if (!passiveDataDict.Contains("type") || !(passiveDataDict["type"] is String)) {
			System.Console.WriteLine("Error : passive.passivedata.type@" + path);
			return;
		}
		String type = (String)passiveDataDict["type"];
		shape = new Shape(shapeVertical, shapeDiagonal, shapeRange, shapeMinRange, shapeMaxRange);
		passive = Passive.StrToPassive(type, title, description, filter, shape, filterIgnore, blockedBy, blockedByIgnore);
		if (passive == null) {
			System.Console.WriteLine("Error : passive.passivedata.type.value@" + path);
			return;
		}

		//Passive.PassiveData.Config
		if (!passiveDataDict.Contains("config") || !(passiveDataDict["config"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : passive.passivedata.config@" + path);
			return;
		}
		Godot.Collections.Dictionary configDict = (Godot.Collections.Dictionary)passiveDataDict["config"];
		if (!passive.Config(configDict)) {
			passive.RmPassive();
			return;
		}

		System.Console.WriteLine("OK !");
		return;
	}

	/// <summary>
	/// Called by BuildTerrains
	/// </summary>
	private void buildSingleTerrain(String path) {
		System.Console.Write("    Building terrain... ");
		if (!System.IO.File.Exists(System.IO.Path.Combine(new String[] {path, "terrain.conf"}))) {
			Console.WriteLine("Error : File missing: " + System.IO.Path.Combine(new String[] {path, "terrain.conf"}));
			return;
		}

		String dirPath = path;
		path = System.IO.Path.Combine(new String[] {path, "terrain.conf"});
		String title = null;
		String description = null;
		EntityPhysical.ETeam team = EntityPhysical.ETeam.Neutral;
		List<String> tag = new List<String>();
		List<Passive> passiveList = new List<Passive>();

		//Those will be used for the animations
		Renderer renderer = Renderer.StrToRenderer(_RendererStr);
		Renderer billboardRenderer = null; //optionally set

		//We open the config file
		File configFile = new File();
		configFile.Open(path, File.ModeFlags.Read);
		JSONParseResult parseResult = JSON.Parse(configFile.GetAsText());
		if (parseResult.Error != Error.Ok) {
			System.Console.WriteLine("\n" + path);
			System.Console.WriteLine(parseResult.ErrorString);
			return;
		}

		///DATAS
		//Root
		if (!(parseResult.Result is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : root@" + path);
			return;
		}
		Godot.Collections.Dictionary dictionary = (Godot.Collections.Dictionary)parseResult.Result;

		//Terrain
		if (!dictionary.Contains("terrain") || !(dictionary["terrain"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : terrain@" + path);
			return;
		}
		Godot.Collections.Dictionary terrainDict = (Godot.Collections.Dictionary)dictionary["terrain"];

		//Terrain.Name
		if (!terrainDict.Contains("name") || !(terrainDict["name"] is String)) {
			System.Console.WriteLine("Error : terrain.name@" + path);
			return;
		}
		title = (String)terrainDict["name"];

		//Terrain.Description
		if (!terrainDict.Contains("description") || !(terrainDict["description"] is String)) {
			System.Console.WriteLine("Error : terrain.description@" + path);
			return;
		}
		description = (String)terrainDict["description"];

		//Terrain.Tags
		if (!terrainDict.Contains("tags") || !(terrainDict["tags"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : terrain.tags@" + path);
			return;
		}
		Godot.Collections.Array tags = (Godot.Collections.Array)terrainDict["tags"];
		foreach (System.Object o in tags) {
			if (!(o is String)) {
				System.Console.WriteLine("Error : terrain.tags.element@" + path);
				return;
			}
			tag.Add((String)o);
		}

		//Terrain.Renderer
		if (!terrainDict.Contains("renderer") || !(terrainDict["renderer"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : terrain.renderer@" + path);
			return;
		}
		Godot.Collections.Dictionary rendererDict = (Godot.Collections.Dictionary)terrainDict["renderer"];
		if (!renderer.Config(rendererDict, dirPath, "terrain")) {
			return;
		}

		//Terrain.BillboardRenderer(optionnal)
		if (terrainDict.Contains("billboardrenderer") && (terrainDict["billboardrenderer"] is Godot.Collections.Dictionary)) {
			rendererDict = (Godot.Collections.Dictionary)terrainDict["billboardrenderer"];
			billboardRenderer = Renderer.StrToRenderer(_RendererStr);
			if (!billboardRenderer.Config(rendererDict, dirPath, "billboard")) {
				return;
			}

		}

		//Terrain.Passives
		if (!terrainDict.Contains("passives") || !(terrainDict["passives"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : terrain.passives@" + path);
			return;
		}
		Godot.Collections.Array passives = (Godot.Collections.Array)terrainDict["passives"];
		foreach (System.Object o in passives) {
			if (!(o is String)) {
				System.Console.WriteLine("Error : terrain.passives.element@" + path);
				return;
			}
			Passive p = Passive.GetPassive((String)o);
			if (p == null) {
				System.Console.WriteLine("Error : terrain.passives.element(could not find passive " + (String)o + ") @" + path);
				return;
			}
			passiveList.Add(p);
		}

		System.Console.WriteLine("Ok !");
		new Terrain(title, description, team, tag, passiveList, renderer, billboardRenderer);
	}

	/// <summary>
	/// Called by BuildMaps
	/// </summary>
	private Map buildSingleMap(String path) {
		System.Console.Write("    Building map... ");
		if(!(System.IO.File.Exists(path) && System.IO.Path.GetExtension(path).Equals(".conf"))) {
			return null;
		}

		//Default values
		String title = "";
		String description = "";
		int length = 20;
		int height = 25;
		List<Player> playerList = new List<Player>();

		//We open the config file
		File configFile = new File();
		configFile.Open(path, File.ModeFlags.Read);
		JSONParseResult parseResult = JSON.Parse(configFile.GetAsText());
		if (parseResult.Error != Error.Ok) {
			System.Console.WriteLine("\n" + path);
			System.Console.WriteLine(parseResult.ErrorString);
			return null;
		}

		///DATAS
		//Root
		if (!(parseResult.Result is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : root@" + path);
			return null;
		}
		Godot.Collections.Dictionary dictionary = (Godot.Collections.Dictionary)parseResult.Result;

		//Terrain
		if (!dictionary.Contains("map") || !(dictionary["map"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : map@" + path);
			return null;
		}
		Godot.Collections.Dictionary mapDict = (Godot.Collections.Dictionary)dictionary["map"];

		//map.Name
		if (!mapDict.Contains("name") || !(mapDict["name"] is String)) {
			System.Console.WriteLine("Error : map.name@" + path);
			return null;
		}
		title = (String)mapDict["name"];

		//map.Description
		if (!mapDict.Contains("description") || !(mapDict["description"] is String)) {
			System.Console.WriteLine("Error : map.description@" + path);
			return null;
		}
		description = (String)mapDict["description"];

		//map.Size
		if (!mapDict.Contains("size") || !(mapDict["size"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : map.size@" + path);
			return null;
		}
		Godot.Collections.Array size = (Godot.Collections.Array)mapDict["size"];
		if (size.Count != 2) {
			System.Console.WriteLine("Error : map.size.count@" + path);
			return null;
		}
		if (!(size[0] is float)) {
			System.Console.WriteLine("Error : map.size.element@" + path);
			return null;
		}
		if (!(size[1] is float)) {
			System.Console.WriteLine("Error : map.size.element@" + path);
			return null;
		}
		height = (int)(float)size[0];
		length = (int)(float)size[1];

		//Map.Teams
		if (!mapDict.Contains("teams") || !(mapDict["teams"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : map.teams@" + path);
			return null;
		}
		Godot.Collections.Array teams = (Godot.Collections.Array)mapDict["teams"];
		if (teams.Count < 2) {
			System.Console.WriteLine("Error : map.teams.count(not enough teams)@" + path);
			return null;
		}
		for (int teamnb = 0; teamnb < teams.Count; teamnb++) { //For each team row {name, units, allies}
			List<Unit> teamUnitList = new List<Unit>();
			System.Object o = teams[teamnb];
			if (!(o is Godot.Collections.Dictionary)) {
				System.Console.WriteLine("Error : map.teams.element@" + path);
				return null;
			}
			Godot.Collections.Dictionary teamsElementDict = (Godot.Collections.Dictionary)o;

			//map.Teams.Element.Name
			if (!teamsElementDict.Contains("name") || !(teamsElementDict["name"] is String)) {
				System.Console.WriteLine("Error : map.teams.element.name@" + path);
				return null;
			}
			String teamName = (String)teamsElementDict["name"];
		
			//map.Teams.Element.Units
			if (!teamsElementDict.Contains("units") || !(teamsElementDict["units"] is Godot.Collections.Array)) {
				System.Console.WriteLine("Error : map.teams.element.units@" + path);
				return null;
			}
			Godot.Collections.Array unitsArray = (Godot.Collections.Array)teamsElementDict["units"];
			foreach (System.Object obj in unitsArray) { //For each unit row of that team {name, count}
				if (!(obj is Godot.Collections.Dictionary)) {
					System.Console.WriteLine("Error : map.teams.element.units.element@" + path);
					return null;
				}
				Godot.Collections.Dictionary unitsElementDict = (Godot.Collections.Dictionary)obj;

				//Map.Teams.Element.Units.Element.Name
				if (!unitsElementDict.Contains("name") || !(unitsElementDict["name"] is String)) {
					System.Console.WriteLine("Error : map.teams.element.units.element.name@" + path);
					return null;
				}
				Unit u = Unit.SearchUnit((String)unitsElementDict["name"]);
				if (u == null) {
					System.Console.WriteLine("Error : map.teams.element.units.element.name.value(unit not found)@" + path);
					return null;
				}

				//Map.Teams.Element.Units.Element.Count
				if (!unitsElementDict.Contains("count") || !(unitsElementDict["count"] is float)) {
					System.Console.WriteLine("Error : map.teams.element.units.element.count@" + path);
					return null;
				}
				for (int i = 0; i < (int)(float)unitsElementDict["count"]; i++) {
					teamUnitList.Add(new Unit(u, (EntityPhysical.ETeam)teamnb));
				}
			}

			//Map.Teams.Element.Allies
			//TODO: alliances


			//Team composition check
			if (teamUnitList.Count < 1) {
				System.Console.WriteLine("Error : map.teams.element(Not enough units)" + path);
				return null;
			}
			playerList.Add(new Player(teamnb, teamUnitList, teamName));
		}


		Console.WriteLine("Ok !");
		return new Map(title, description, height, length, _defaultTile, new Scenario(playerList));
	}


	/// <summary>
	/// Creates a single <see cref="Unit"/> instance based on its config file, makes it neutral. This Unit should then be cloned to each team that owns it
	/// </summary>
	/// <param name="path"> the path of the unit</param>
	/// <returns> null if an error occured, a <see cref="Unit"/> otherwise </returns>
	private Unit buildSingleUnit (String path) {
		System.Console.Write("    Building unit...");
		//First we check the dependencies
		if (!System.IO.File.Exists(System.IO.Path.Combine(new String[] {path, "unit.conf"}))) {
			Console.WriteLine("File missing: " + System.IO.Path.Combine(new String[] {path, "unit.conf"}));
			return null;
		}

		String dirPath = path;
		path = System.IO.Path.Combine(new String[] {path, "unit.conf"});

		//Those will be used to build the unit
		String title = null;
		String description = null;
		EntityPhysical.ETeam team = (EntityPhysical.ETeam)(-1);
		int defense = -1;
		Shape movement = null;
		List<String> tag = new List<String>();
		List<Attack> attackList = new List<Attack>();
		List<Passive> passiveList = new List<Passive>();
		List<String> blockedBy = new List<String>();
		Action.ETarget blockedByIgnore = (Action.ETarget)(-1);

		//Those will be used for the animations
		Renderer renderer = Renderer.StrToRenderer(_RendererStr);

		
		//We open the config file
		File configFile = new File();
		configFile.Open(path, File.ModeFlags.Read);
		JSONParseResult parseResult = JSON.Parse(configFile.GetAsText());
		if (parseResult.Error != Error.Ok) {
			System.Console.WriteLine("\n" + path);
			System.Console.WriteLine(parseResult.ErrorString);
			return null;
		}

		///DATAS
		//Root
		if (!(parseResult.Result is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : root@" + path);
			return null;
		}
		Godot.Collections.Dictionary dictionary = (Godot.Collections.Dictionary)parseResult.Result;

		//Unit
		if (!dictionary.Contains("unit") || !(dictionary["unit"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : unit@" + path);
			return null;
		}
		Godot.Collections.Dictionary unitDict = (Godot.Collections.Dictionary)dictionary["unit"];

		//Unit.Name
		if (!unitDict.Contains("name") || !(unitDict["name"] is String)) {
			System.Console.WriteLine("Error : unit.name@" + path);
			return null;
		}
		title = (String)unitDict["name"];

		//Unit.Description
		if (!unitDict.Contains("description") || !(unitDict["description"] is String)) {
			System.Console.WriteLine("Error : unit.description@" + path);
			return null;
		}
		description = (String)unitDict["description"];

		//Unit.Defense
		if (!unitDict.Contains("defense") || !(unitDict["defense"] is float)) {
			System.Console.WriteLine("Error : unit.defense@" + path);
			return null;
		}
		defense = (int)(float)unitDict["defense"];

		//Unit.Tags
		if (!unitDict.Contains("tags") || !(unitDict["tags"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : unit.tags@" + path);
			return null;
		}
		Godot.Collections.Array tags = (Godot.Collections.Array)unitDict["tags"];
		foreach (System.Object o in tags) {
			if (!(o is String)) {
				System.Console.WriteLine("Error : unit.tags.element@" + path);
				return null;
			}
			tag.Add((String)o);
		}

		//Unit.Move
		if (!unitDict.Contains("move") || !(unitDict["move"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : unit.move@" + path);
			return null;
		}
		Godot.Collections.Dictionary moveDict = (Godot.Collections.Dictionary)unitDict["move"];

		//Unit.Move.Vertical
		if (!moveDict.Contains("vertical") || !(moveDict["vertical"] is bool)) {
			System.Console.WriteLine("Error : unit.move.vertical@" + path);
			return null;
		}
		bool shapeVertical = (bool)moveDict["vertical"];

		//Unit.Move.Diagonal
		if (!moveDict.Contains("diagonal") || !(moveDict["diagonal"] is bool)) {
			System.Console.WriteLine("Error : unit.move.diagonal@" + path);
			return null;
		}
		bool shapeDiagonal = (bool)moveDict["diagonal"];

		//Unit.Move.Range
		if (!moveDict.Contains("range") || !(moveDict["range"] is float)) {
			System.Console.WriteLine("Error : unit.move.range@" + path);
			return null;
		}
		int shapeRange = (int)(float)moveDict["range"];


		//Unit.Move.MinRange
		if (!moveDict.Contains("min_range") || !(moveDict["min_range"] is float)) {
			System.Console.WriteLine("Error : unit.move.min_range@" + path);
			return null;
		}
		int shapeMinRange = (int)(float)moveDict["min_range"];

		//Unit.Move.MaxRange
		if (!moveDict.Contains("max_range") || !(moveDict["max_range"] is float)) {
			System.Console.WriteLine("Error : unit.move.max_range@" + path);
			return null;
		}
		int shapeMaxRange = (int)(float)moveDict["max_range"];

		//Unit.BlockedBy
		if (!unitDict.Contains("blockedby") || !(unitDict["blockedby"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : attack.blockedby@" + path);
			return null;
		}
		Godot.Collections.Dictionary blockedByDict = (Godot.Collections.Dictionary)unitDict["blockedby"];

		//Unit.BlockedBy.Tags
		if (!blockedByDict.Contains("tags") || !(blockedByDict["tags"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : attack.blockedby.tags@" + path);
			return null;
		}
		tags = (Godot.Collections.Array)blockedByDict["tags"];
		foreach (System.Object o in tags) {
			if (!(o is String)) {
				System.Console.WriteLine("Error : attack.blockedby.tags.element@" + path);
				return null;
			}
			blockedBy.Add((String)o);
		}

		//Unit.BlockedBy.Ignore
		if (!blockedByDict.Contains("ignore") || !(blockedByDict["ignore"] is String)) {
			System.Console.WriteLine("Error : attack.blockedby.ignore@" + path);
			return null;
		}
		String ignore = (String)blockedByDict["ignore"];
		blockedByIgnore = Action.StrToETarget(ignore);
		if ((int)blockedByIgnore == -1) {
			System.Console.WriteLine("Error : attack.blockedby.ignore.value@" + path);
			return null;
		}

		//Unit.Renderer
		if (!unitDict.Contains("renderer") || !(unitDict["renderer"] is Godot.Collections.Dictionary)) {
			System.Console.WriteLine("Error : unit.renderer@" + path);
			return null;
		}
		Godot.Collections.Dictionary rendererDict = (Godot.Collections.Dictionary)unitDict["renderer"];
		if (!renderer.Config(rendererDict, dirPath, "idle")) {
			return null;
		}

		//Unit.Attacks
		if (!unitDict.Contains("attacks") || !(unitDict["attacks"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : unit.attacks@" + path);
			return null;
		}
		Godot.Collections.Array attacks = (Godot.Collections.Array)unitDict["attacks"];
		foreach (System.Object o in attacks) {
			if (!(o is String)) {
				System.Console.WriteLine("Error : unit.attacks.element@" + path);
				return null;
			}
			Attack a = Attack.GetAttack((String)o);
			if (a == null) {
				System.Console.WriteLine("Error : unit.attacks.element(could not find attack " + (String)o + ") @" + path);
				return null;
			}
			attackList.Add(Attack.GetAttack((String)o));
		}

		//Unit.Passives
		if (!unitDict.Contains("passives") || !(unitDict["passives"] is Godot.Collections.Array)) {
			System.Console.WriteLine("Error : unit.passives@" + path);
			return null;
		}
		Godot.Collections.Array passives = (Godot.Collections.Array)unitDict["passives"];
		foreach (System.Object o in passives) {
			if (!(o is String)) {
				System.Console.WriteLine("Error : unit.passives.element@" + path);
				return null;
			}
			Passive p = Passive.GetPassive((String)o);
			if (p == null) {
				System.Console.WriteLine("Error : unit.passives.element(could not find passive " + (String)o + ") @" + path);
				return null;
			}
			passiveList.Add(Passive.GetPassive((String)o));
		}

		System.Console.WriteLine("Ok !");
		movement = new Shape(shapeVertical, shapeDiagonal, shapeRange, shapeMinRange, shapeMaxRange);
		return new Unit(title, description, team, defense, movement, tag, attackList, passiveList, renderer, blockedBy, _defaultTextRenderer);
	}

	//  private void buildTextRenderer(String path){
	// 	System.Console.Write("    Building TextRenderer... ");
	// 	if(!(System.IO.File.Exists(path) && System.IO.Path.GetExtension(path).Equals(".conf"))) {
	// 		return null;
	// 	}

	// 	//Default values
	// 	String font = "ComicSansMs";
	// 	int size = 12;
	// 	List<int> info = new List<int>();
	// 	List<int> danger = new List<int>();
	// 	List<int> help = new List<int>();
	// 	int red= 250;
	// 	int yellow= 0;
	// 	int blue= 0;

	// 	System.IO.StreamReader sr = new System.IO.StreamReader(path);

	// 	while (!sr.EndOfStream) {
	// 		String line = sr.ReadLine();
	// 		//Removing the spaces and tabs used for indentation, splitting the line into command[0] the command name, and command[1] the parameters
	// 		line = line.TrimStart().TrimEnd();
	// 		String[] command = line.Split(':');
	// 		String[] param;
	// 		if (command.Length == 2) {
	// 			command[0] = command[0].TrimStart().TrimEnd();
	// 			command[1] = command[1].TrimStart().TrimEnd();
	// 			switch(command[0]) {
	// 				case "FONT":
	// 					font = command[1];
	// 					break;

	// 				case "SIZE":
	// 					size = command[1];
	// 					break;

	// 				case "INFO":
	// 					param = command[1].Split(",");
	// 					for (int i = 0; i < 3; i++) {
	// 						param[i] = param[i].TrimEnd().TrimStart();
	// 					}
	// 					red = int.Parse(param[0]);
	// 					yellow = int.Parse(param[1]);
	// 					blue  = int.Parse(param[2]);
	// 					break;

	// 				case "DANGER" :
	// 					param = command[1].Split(",");
	// 					for (int i = 0; i < 3; i++) {
	// 						param[i] = param[i].TrimEnd().TrimStart();
	// 					}
	// 					red = int.Parse(param[0]);
	// 					yellow = int.Parse(param[1]);
	// 					blue  = int.Parse(param[2]);
	// 					break;


	// 				case "HELP":
	// 					param = command[1].Split(",");
	// 					for (int i = 0; i < 3; i++) {
	// 						param[i] = param[i].TrimEnd().TrimStart();
	// 					}
	// 					red = int.Parse(param[0]);
	// 					yellow = int.Parse(param[1]);
	// 					blue  = int.Parse(param[2]);
	// 					break;
	// 			}
	// 		}
	// 	}

	//    System.Console.WriteLine("Ok !");
	// 	return new textRenderer(font, size, info, danger, help);
	// }
}
