#   Kriegspiel : le jeu de la guerre
#   Copyright (C) 2019  SURO François (suro@lirmm.fr)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


extends Camera
var cameraGroup
var dragging = false
var rotating = false


func _ready():
	cameraGroup = get_node("../../CameraGroup")
	pass

func _process(delta):
	look_at(cameraGroup.get_transform().origin, Vector3(0,1,0))
	pass

func _input(ev):
	if ev is InputEventMouseButton and ev.button_index == BUTTON_RIGHT and ev.is_pressed() and not dragging and not rotating:
		dragging = true
	if dragging and ev is InputEventMouseMotion:
		var orient = cameraGroup.get_transform().origin - get_camera_transform().origin
		orient.y = 0
		orient = orient.normalized()
		var perp = orient.rotated(Vector3(0,1,0),PI/2.0)
		orient = orient * ev.relative.y
		perp = perp * ev.relative.x
		cameraGroup.translate(orient / 50)
		cameraGroup.translate(perp / 50)
	if dragging and ev is InputEventMouseButton and ev.button_index == BUTTON_RIGHT:
		if not ev.is_pressed():
			dragging = false

	if ev is InputEventMouseButton and ev.button_index == BUTTON_MIDDLE and ev.is_pressed() and not rotating and not dragging:
		rotating = true
	if rotating and ev is InputEventMouseMotion:
		var prevDist = get_camera_transform().origin.distance_to(cameraGroup.get_transform().origin)
		translate_object_local(Vector3(-ev.relative.x,0,0) / 20)
		var vec = cameraGroup.get_transform().origin - get_camera_transform().origin
		var vecGrd = Vector3(vec.x,0,vec.z)
		var angle = vec.angle_to(vecGrd)
		if (angle > PI/6 and ev.relative.y < 0) or (angle < PI/3 and ev.relative.y > 0):
			translate_object_local(Vector3(0,ev.relative.y,0) / 20)
		var Dist = prevDist -  get_camera_transform().origin.distance_to(cameraGroup.get_transform().origin)
		look_at(cameraGroup.get_transform().origin, Vector3(0,1,0))
		translate_object_local(Vector3(0,0,Dist) / 20)
	if rotating and ev is InputEventMouseButton and ev.button_index == BUTTON_MIDDLE:
		if not ev.is_pressed():
			rotating = false

	if ev is InputEventMouseButton :
		if ev.button_index == BUTTON_WHEEL_UP:
			if get_camera_transform().origin.distance_to(cameraGroup.get_transform().origin) > 2:
				translate_object_local(Vector3(0,0,-0.5))
		if ev.button_index == BUTTON_WHEEL_DOWN:
			if get_camera_transform().origin.distance_to(cameraGroup.get_transform().origin) < 70:
				translate_object_local(Vector3(0,0,0.5))
	pass
